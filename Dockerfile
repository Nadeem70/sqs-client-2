FROM node:13.2.0-alpine

WORKDIR /opt/sqs

ADD . /opt/sqs

RUN npm install
RUN apk update && apk add bash
CMD ["node", "index.js"]